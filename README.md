# weather

Models de dades del projecte VLCi basats en els data models Fiware: https://github.com/smart-data-models/dataModel.Weather

## Listado de modelos de datos

Los siguientes tipos de entidad están disponibles:

- [WeatherForecast](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherForecast/README.md). Descripción armonizada de una previsión meteorológica.

- [WeatherObserved](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/README.md). Una observación de las condiciones meteorológicas en un lugar y momento determinados.

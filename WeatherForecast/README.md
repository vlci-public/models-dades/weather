Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/weather/blob/main/WeatherForecast/spec.md) que se tiene. Se modelan las predicciones meteorológicas producidas por el proyecto CityClim. Cada día se producen nuevas predicciones de las próximas 48h, con una periodicidad de 60 minutos.


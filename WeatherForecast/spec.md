**Índice**  

 [[_TOC_]]  
  

# Entidad: WeatherForecast

[Basado en la entidad Fiware WeatherForecast](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherForecast)

Descripción global: **Descripción armonizada de una previsión meteorológica.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id[text]`: Identificador único de la entidad
- `type[text]`: NGSI Tipo de entidad
- `TimeInstant[date-time]`: Timestamp de la actualización de la entidad a través del Agente IoT

##### Atributos descriptivos de la entidad

- `name[text]`: El nombre de donde se ubica la previsión meteorológica.
- `location[geo:json]`: Referencia Geojson al elemento. Será un Point.
- `address[text]`: La dirección postal.
- `project[text]`: Proyecto al que pertenece esta entidad.

##### Atributos descriptivos de la entidad (OPCIONALES)

- `dataProvider[text]`: Una secuencia de caracteres que identifica al proveedor de la entidad de datos armonizada.

##### Atributos de la medición

- `dateIssued[date-time]`: Fecha y hora de emisión de la previsión en formato ISO8601 UTC. 
- `validFrom[date-time]`: Fecha y hora de inicio del periodo de validez en formato ISO8601 UTC.
- `validTo[date-time]`: Fecha y hora de finalización del periodo de validez en formato ISO8601 UTC.
- `temperatureSurface`: Temperatura en superficie. Se mide en grados Celsius.
- `temperature2m`: Temperatura a 2 metros de altura. Se mide en grados Celsius.
- `windSpeed`: Velocidad del viento a 10 metros de altura. Se mide en m/s.
- `dewPoint2m`: Punto de rocío a 2 metros de altura. Se mide en grados Celsius.

##### Atributos para la monitorización

- `operationalStatus[text]`: Posibles valores: ok, noData
- `inactive[boolean]`: Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner[text]`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail[text]`: Email del responsable técnico de esta entidad
- `serviceOwner[text]`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail[text]`: Email de la persona del servicio municipal de contacto para esta entidad

## Lista de entidades que implementan este modelo (1 entidad)

|   Subservicio  | ID Entidad         |
| -------------- | ------------------ |
| /MedioAmbiente | cityclim_point_id  |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/cityclim_point_id' \
--header 'Fiware-Service: sc_vlci_int' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'

{
    "id": "cityclim_point_id",
    "type": "WeatherForecast",
    "TimeInstant": {
        "type": "DateTime",
        "value": "2024-02-26T13:44:16.312Z",
        "metadata": {}
    },
    "address": {
        "type": "Text",
        "value": "TBD",
        "metadata": {}
    },
    "dataProvider": {
        "type": "Text",
        "value": "OHB Digital",
        "metadata": {}
    },
    "dateIssued": {
        "type": "DateTime",
        "value": "2024-02-22T10:40:01.000Z",
        "metadata": {}
    },
    "dewPoint2m": {
        "type": "Number",
        "value": 22.12,
        "metadata": {}
    },
    "inactive": {
        "type": "Boolean",
        "value": false,
        "metadata": {}
    },
    "location": {
        "type": "geo:json",
        "value": {
            "coordinates": [
                -0.376964822,
                39.46979001
            ],
            "type": "Point"
        },
        "metadata": {}
    },
    "maintenanceOwner": {
        "type": "Text",
        "value": "Ingo Schoolmann",
        "metadata": {}
    },
    "maintenanceOwnerEmail": {
        "type": "Text",
        "value": "ingo.schoolmann@ohb-ds.de",
        "metadata": {}
    },
    "name": {
        "type": "Text",
        "value": "TBD",
        "metadata": {}
    },
    "operationalStatus": {
        "type": "Text",
        "value": "ok",
        "metadata": {}
    },
    "project": {
        "type": "Text",
        "value": "CityClim",
        "metadata": {}
    },
    "serviceOwner": {
        "type": "Text",
        "value": "Víctor Sancho Fernández",
        "metadata": {}
    },
    "serviceOwnerEmail": {
        "type": "Text",
        "value": "vsanchofe@valencia.es",
        "metadata": {}
    },
    "temperature2m": {
        "type": "Number",
        "value": 15.23,
        "metadata": {}
    },
    "temperatureSurface": {
        "type": "Number",
        "value": 16.02,
        "metadata": {}
    },
    "validFrom": {
        "type": "DateTime",
        "value": "2024-02-22T11:00:00.000Z",
        "metadata": {}
    },
    "validTo": {
        "type": "DateTime",
        "value": "2024-02-22T12:00:00.000Z",
        "metadata": {}
    },
    "windSpeed": {
        "type": "Number",
        "value": 2.23,
        "metadata": {}
    }
}
```

# Atributos que van al GIS para su representación en una capa
- N/A

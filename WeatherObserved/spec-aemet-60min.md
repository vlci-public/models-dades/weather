**Índice**  

 [[_TOC_]]  
  

# Entidad: WeatherObserved

[Basado en la entidad Fiware WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherObserved)

Descripción global: **Una observación de las condiciones del clima en un lugar y momento determinados.**  

Los datos son proporcionados por AEMET y se actualizan cada hora.

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Lista de propiedades

### Atributos de la entidad context broker

- `id`: Identificador único de la entidad.
- `type`: NGSI Tipo de entidad.

### Atributos descriptivos de la entidad

- `name`: El nombre de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `description`: Una descripción de este artículo.
- `TimeInstant`: Permite rastrear el momento preciso asociado con los estados de los atributos de la entidad o el valor de cualquier atributo específico.
- `refPointOfInterest`: Una referencia a un punto de interés (normalmente una estación de clima) asociada a esta observación.
- `project`: Proyecto al que pertenece esta entidad.

### Atributos descriptivos de la entidad (OPCIONALES)

- Ninguno

### Atributos de la medición

- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: Europe/Madrid.
- `dateObservedGMT0`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: UTC o Hora Zulu o GMT+0.
- `latitud`: Coordenada de latitud.
- `longitud`: Coordenada de longitud.
- `altitude`: Altitud sobre el nivel del mar.
- `precipitationValue`: Nivel de precipitación observado.
- `relativeHumidityValue`: Humedad relativa del aire observada.
- `temperatureValue`: Temperatura del aire observada.
- `temperatureMaxValue`: Temperatura máxima observada.
- `temperatureMinValue`: Temperatura mínima observada.
- `windDirectionValue`: La dirección del viento observada.
- `windDirectionMaxValue`: Valor máximo de la dirección del viento.
- `windDirectionUltraValue`: Valor ultra de la dirección del viento.
- `windDirectionMaxUltraValue`: Valor máximo ultra de la dirección del viento.
- `windSpeedValue`: La velocidad del viento observada.
- `windSpeedMaxValue`: Valor máximo de la velocidad del viento.
- `windSpeedUltraValue`: Valor ultra de la velocidad del viento.
- `windSpeedUltraMaxValue`: Valor máximo ultra de la velocidad del viento.
- `insolationValue`: Valor de insolación.
- `cloudCoverValue`: Cobertura de nubes.
- `visibilityValue`: Visibilidad.

### Atributos estáticos de la medición

- `precipitationType`: Tipo de unidad para el nivel de precipitación (mm).
- `relativeHumidityType`: Tipo de unidad para la humedad relativa (%).
- `temperatureType`: Tipo de unidad para la temperatura (°C).
- `temperatureMaxType`: Tipo de unidad para la temperatura máxima (°C).
- `temperatureMinType`: Tipo de unidad para la temperatura mínima (°C).
- `windDirectionType`: Tipo de unidad para la dirección del viento (grados).
- `windDirectionMaxType`: Tipo de unidad para el valor máximo de la dirección del viento (grados).
- `windDirectionUltraType`: Tipo de unidad para el valor ultra de la dirección del viento (grados).
- `windDirectionMaxUltraType`: Tipo de unidad para el valor máximo ultra de la dirección del viento (grados).
- `windSpeedType`: Tipo de unidad para la velocidad del viento (m/s).
- `windSpeedUltraType`: Tipo de unidad para la velocidad ultra del viento (m/s).
- `windSpeedMaxType`: Tipo de unidad para el valor máximo de la velocidad del viento (m/s).
- `windSpeedMaxUltraType`: Tipo de unidad para el valor máximo ultra de la velocidad del viento (m/s).
- `insolationType`: Tipo de unidad para el valor de insolación (Número).
- `cloudCoverType`: Tipo de unidad para la cobertura de nubes (%).
- `visibilityType`: Tipo de unidad para la visibilidad (Km).

### Atributos para realizar cálculos / lógica de negocio

- Ninguno

### Atributos para el GIS / Representar gráficamente la entidad

- Ninguno

### Atributos para la monitorización

- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData.
- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad                 |
| -------------- | -------------------------- |
| /MedioAmbiente | W03_VALENCIAAEROPUERTO_10m |
| /MedioAmbiente | W04_VALENCIADT_10m         |
| /MedioAmbiente | W05_VALENCIA_UPV_10m       |

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>

{
"method": "POST",
"path": "/",
"query": {},
"client_ip": "XX.XX.XX.XX",
"url": "XXXXX",
"headers": {
"host": "XXXXX",
"content-length": "6022",
"user-agent": "orion/3.10.1 libcurl/7.74.0",
"fiware-service": "XXXXX",
"fiware-servicepath": "/MedioAmbiente",
"x-auth-token": "XXXXX",
"accept": "application/json",
"content-type": "application/json; charset=utf-8",
"fiware-correlator": "xxx; cbnotif=1",
"ngsiv2-attrsformat": "normalized"
},
"bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"prueba0019\",\"type\":\"WeatherObserved\",\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\",\"metadata\":{}},\"altitude\":{\"type\":\"Number\",\"value\":50.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"cloudCoverType\":{\"type\":\"Text\",\"value\":\"%\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"cloudCoverValue\":{\"type\":\"Number\",\"value\":40,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2023-11-14T12:00:00.000Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"insolationType\":{\"type\":\"Text\",\"value\":\"Numero\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"insolationValue\":{\"type\":\"Number\",\"value\":500,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-73.987,40.756],\"type\":\"Point\"},\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"csuarezj@aemet.es\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"name\":{\"type\":\"Text\",\"value\":\"prueba0019\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"Operational\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"precipitationType\":{\"type\":\"Text\",\"value\":\"mm\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"precipitationValue\":{\"type\":\"Number\",\"value\":2.34,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"project\":{\"type\":\"Text\",\"value\":\"CityClim Aemet\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"City Center\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"relativeHumidityType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"relativeHumidityValue\":{\"type\":\"Number\",\"value\":70.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"Victor Sancho\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"temperatureMaxType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"temperatureMaxValue\":{\"type\":\"Number\",\"value\":27.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"temperatureMinType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"temperatureMinValue\":{\"type\":\"Number\",\"value\":24.3,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"temperatureType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"temperatureValue\":{\"type\":\"Number\",\"value\":25.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"visibilityType\":{\"type\":\"Text\",\"value\":\"km\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"visibilityValue\":{\"type\":\"Number\",\"value\":10,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionMaxType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionMaxUltraType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionMaxUltraValue\":{\"type\":\"Number\",\"value\":125,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionMaxValue\":{\"type\":\"Number\",\"value\":135,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionUltraType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionUltraValue\":{\"type\":\"Number\",\"value\":110,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windDirectionValue\":{\"type\":\"Number\",\"value\":120,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedMaxType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedMaxUltraType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedMaxValue\":{\"type\":\"Number\",\"value\":12,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedUltraMaxValue\":{\"type\":\"Number\",\"value\":10.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedUltraType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedUltraValue\":{\"type\":\"Number\",\"value\":7.8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}},\"windSpeedValue\":{\"type\":\"Number\",\"value\":8.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-23T10:25:21.583Z\"}}}}]}",
"body": {
"subscriptionId": "XXXXX",
"data": [
{
"id": "prueba0019",
"type": "WeatherObserved",
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z",
"metadata": {}
},
"altitude": {
"type": "Number",
"value": 50.5,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"cloudCoverType": {
"type": "Text",
"value": "%",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"cloudCoverValue": {
"type": "Number",
"value": 40,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"dateObserved": {
"type": "DateTime",
"value": "2023-11-14T12:00:00.000Z",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"insolationType": {
"type": "Text",
"value": "Numero",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"insolationValue": {
"type": "Number",
"value": 500,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"location": {
"type": "geo:json",
"value": {
"coordinates": [
-73.987,
40.756
],
"type": "Point"
},
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"maintenanceOwner": {
"type": "Text",
"value": "XXXXX",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"maintenanceOwnerEmail": {
"type": "Text",
"value": "csuarezj@aemet.es",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"name": {
"type": "Text",
"value": "prueba0019",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"operationalStatus": {
"type": "Text",
"value": "Operational",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"precipitationType": {
"type": "Text",
"value": "mm",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"precipitationValue": {
"type": "Number",
"value": 2.34,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"project": {
"type": "Text",
"value": "CityClim Aemet",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"refPointOfInterest": {
"type": "Text",
"value": "City Center",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"relativeHumidityType": {
"type": "Text",
"value": "ºC",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"relativeHumidityValue": {
"type": "Number",
"value": 70.5,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"serviceOwner": {
"type": "Text",
"value": "Victor Sancho",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"serviceOwnerEmail": {
"type": "Text",
"value": "XXXXX",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"temperatureMaxType": {
"type": "Text",
"value": "ºC",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"temperatureMaxValue": {
"type": "Number",
"value": 27.5,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"temperatureMinType": {
"type": "Text",
"value": "ºC",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"temperatureMinValue": {
"type": "Number",
"value": 24.3,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"temperatureType": {
"type": "Text",
"value": "ºC",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"temperatureValue": {
"type": "Number",
"value": 25.5,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"visibilityType": {
"type": "Text",
"value": "km",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"visibilityValue": {
"type": "Number",
"value": 10,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionMaxType": {
"type": "Text",
"value": "grados",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionMaxUltraType": {
"type": "Text",
"value": "grados",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionMaxUltraValue": {
"type": "Number",
"value": 125,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionMaxValue": {
"type": "Number",
"value": 135,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionType": {
"type": "Text",
"value": "grados",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionUltraType": {
"type": "Text",
"value": "grados",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionUltraValue": {
"type": "Number",
"value": 110,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windDirectionValue": {
"type": "Number",
"value": 120,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedMaxType": {
"type": "Text",
"value": "m/s",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedMaxUltraType": {
"type": "Text",
"value": "m/s",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedMaxValue": {
"type": "Number",
"value": 12,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedType": {
"type": "Text",
"value": "m/s",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedUltraMaxValue": {
"type": "Number",
"value": 10.5,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedUltraType": {
"type": "Text",
"value": "m/s",
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedUltraValue": {
"type": "Number",
"value": 7.8,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
},
"windSpeedValue": {
"type": "Number",
"value": 8.5,
"metadata": {
"TimeInstant": {
"type": "DateTime",
"value": "2023-11-23T10:25:21.583Z"
}
}
}
}
]
}
}

**Índice**  

 [[_TOC_]]  
  

# Entidad: WeatherObserved

[Basado en la entidad Fiware WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherObserved)

Descripción global: **Una observación de las condiciones del clima en un lugar y momento determinados.**  

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker
- `id[string]`: Identificador único de la entidad.
- `type[string]`: NGSI Tipo de entidad.

##### Atributos descriptivos de la entidad
- `name[string]`: El nombre de este artículo.
- `location[geo:json]`: Referencia Geojson al elemento. Tipo Point.
- `address[string]`: La direccion postal.
- `project[string]`: Proyecto al que pertenece esta entidad.
- `description[string]`: Una descripción de este artículo. 

##### Atributos de la medición
- `dateObserved[dateTime]`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: Europe/Madrid. Las mediciones son 5 minutales
- `atmosphericPressure[number]`: La presión atmosférica observada. Se mide en hPa.
- `relativeHumidity[number]`: Humedad relativa del aire observada. Se mide en %.
- `temperature[number]`: Temperatura del aire observada. Se mide en ºC.
- `dewPoint[number]`: Punto de rocío. Se mide en ºC.

##### Atributos de validación de las medidas
Cada atributo de medición tiene un flag asociado que describe su validez. Posibles valores:

| Flag | Descripción                                          |
|------|------------------------------------------------------|
|  T   |  Dato en bruto                                       |
|  TI  |  Dato auto invalidado por el software de Kunak Cloud |
|  I   |  Dato invalidado manualmente por un operador         |
|  V   |  Dato validado manualmente por un operador           |
|  O   |  Dato corregido en base a una calibración manual     |

- `atmosphericPressureFlag`
- `relativeHumidityFlag`
- `temperatureFlag`
- `dewPointFlag`

##### Atributos para la monitorización
- `operationalStatus[string]`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData
- `inactive[boolean]`: Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner[string]`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail[string]`: Email del responsable técnico de esta entidad
- `serviceOwner[string]`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail[string]`: Email de la persona del servicio municipal de contacto para esta entidad

## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad        |
|----------------|-------------------|
| /MedioAmbiente |   ECS_clima_5m    |

Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/spec.md) que se tiene. La entidad integrada desde la plataforma Sentilo, de la diputación de Valencia sigue este modelo.

Otras especificaciones históricas
===
Entidades de BSG via webservice, con datos validados 10 minutales [Especificación BSG 10 minutal](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/spec-bsg-10min.md)

Entidades de AEMET (3 estaciones), con datos diezminutales enviados cada hora. [Especificación AEMET 10 minutal](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/spec-aemet-10min.md)

Entidades de AEMET (3 estaciones), con datos horarios enviados cada hora. [Especificación AEMET 60 minutal](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/spec-aemet-60min.md)

Entidades de Kunak vía agentes IoT, con datos 5 minutales enviados cada hora [Especificación Kunak 5 minutal](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/spec-kunak-5min.md)

Entidades de Kunak vía agentes IoT, con datos de la última hora enviados cada hora [Especificación Kunak 60 minutal](https://gitlab.com/vlci-public/models-dades/weather/-/blob/main/WeatherObserved/spec-kunak-60min.md)

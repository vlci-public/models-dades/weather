**Índice**  

 [[_TOC_]]  
  

# Entidad: WeatherObserved

[Basado en la entidad Fiware WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherObserved)

Descripción global: **Una observación de las condiciones del clima en un lugar y momento determinados.**  

Los datos son proporcionados por AEMET y se actualizan cada diez minutos.

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Lista de propiedades

### Atributos de la entidad context broker

- `id`: Identificador único de la entidad.
- `type`: Tipo de entidad NGSI.

### Atributos descriptivos de la entidad

- `name`: Nombre codificado por parte de AEMET de la estación meteo.
- `location`: Referencia GeoJSON al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `description`: Descripción de este artículo.
- `TimeInstant`: Permite rastrear el momento preciso asociado con los estados de los atributos de la entidad o el valor de cualquier atributo específico.
- `refPointOfInterest`: Referencia a un punto de interés (normalmente una estación de clima) asociada a esta observación.
- `project`: Proyecto al que pertenece esta entidad.

### Atributos descriptivos de la entidad (OPCIONALES)

- Ninguno

### Atributos de la medición

- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: Europe/Madrid.
- `dateObservedGMT0`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: UTC o Hora Zulu o GMT+0.
- `altitude`: Número que representa la altitud.
- `atmosphericPressureValue`: Presión atmosférica observada.
- `atmosphericPressureNMARValue`: Presión atmosférica ajustada NMAR.
- `precipitationValue`: Nivel de precipitación observado.
- `precipitationAccumulatedValue`: Valor acumulado de precipitación.
- `precipitationAccumulatedLiquidValue`: Valor acumulado de precipitación líquida.
- `precipitationAccumulatedSolidValue`: Valor acumulado de precipitación sólida.
- `relativeHumidityValue`: Humedad relativa del aire observada.
- `temperatureValue`: Temperatura del aire observada.
- `temperatureDewPointValue`: Temperatura de rocío.
- `temperatureMax1hValue`: Temperatura máxima en la última hora.
- `temperaturaMax10mValue`: Temperatura máxima en los últimos 10 minutos.
- `temperatureMin1hValue`: Temperatura mínima en la última hora.
- `temperaturaMin10mValue`: Temperatura mínima en los últimos 10 minutos.
- `temperatureUnder100cmValue`: Temperatura bajo 100 cm.
- `temperatureUnder10cmValue`: Temperatura bajo 10 cm.
- `temperatureUnder20cmValue`: Temperatura bajo 20 cm.
- `temperatureUnder50cmValue`: Temperatura bajo 50 cm.
- `temperatureUnder5cmValue`: Temperatura bajo 5 cm.
- `windDirectionValue`: Dirección del viento observada.
- `windDirectionMaxValue`: Valor máximo de la dirección del viento.
- `windDirectionDesviationValue`: Desviación de la dirección del viento.
- `windDirectionUltraValue`: Valor ultra de la dirección del viento.
- `windDirectionMaxUltraValue`: Valor máximo ultra de la dirección del viento.
- `windDirectionUltraDesviationValue`: Desviación ultra de la dirección del viento.
- `windSpeedValue`: Velocidad del viento observada.
- `windSpeedMaxValue`: Valor máximo de la velocidad del viento.
- `windSpeedDesviationValue`: Desviación de la velocidad del viento.
- `windSpeedUltraValue`: Valor ultra de la velocidad del viento.
- `windSpeedUltraMaxValue`: Valor máximo ultra de la velocidad del viento.
- `windSpeedDesviationUltraValue`: Desviación ultra de la velocidad del viento.
- `pressureTendencyValue`: Tendencia de presión.
- `windPathValue`: Camino del viento.
- `geopotentialHeightValue`: Altura geopotencial.
- `reductionPressure700Value`: Reducción de presión a 700 hPa.
- `reductionPressure850Value`: Reducción de presión a 850 hPa.
- `reductionPressure925Value`: Reducción de presión a 925 hPa.
- `steamTensionValue`: Tensión de vapor.
- `insolationValue`: Valor de insolación.
- `CloudCoverValue`: Cobertura de nubes.
- `visibilityValue`: Visibilidad.
- `snowHeightValue`: Altura de nieve.
- `snowHeightEstimatedValue`: Altura de nieve estimada.
- `lightningValue`: Actividad de relámpagos.

### Atributos descriptivos de la medición

- `atmosphericNmarPressureType`: Nombre para la presión atmosférica.
- `atmosphericPressureType`: Unidad de medida para mostrar el valor de la presión atmosférica.
- `precipitationType`: Unidad de medida para mostrar el valor de la precipitación.
- `precipitationAccumulatedType`: Unidad de medida para mostrar el valor acumulado de precipitación.
- `precipitationAccumulatedLiquidType`: Unidad de medida para mostrar el valor acumulado de precipitación líquida.
- `precipitationAccumulatedSolidType`: Unidad de medida utilizada para mostrar el valor del acumulado de precipitación sólida, expresado en milímetros (mm).
- `temperatureType`: Unidad de medida utilizada para mostrar el valor de la temperatura del aire, expresado en grados Celsius (ºC).
- `temperatureDewPointType`: Unidad de medida utilizada para mostrar el valor de la temperatura de rocío, expresado en grados Celsius (ºC).
- `temperatureMax1hType`: Unidad de medida utilizada para mostrar el valor de la temperatura máxima en la última hora, expresado en grados Celsius (ºC).
- `temperatureMin1hType`: Unidad de medida utilizada para mostrar el valor de la temperatura mínima en la última hora, expresado en grados Celsius (ºC).
- `temperatureMin10mType`: Unidad de medida utilizada para mostrar el valor de la temperatura mínima en los últimos 10 minutos, expresado en grados Celsius (ºC).
- `temperatureSoilType`: Unidad de medida utilizada para mostrar el valor de la temperatura del suelo, expresado en grados Celsius (ºC).
- `temperatureUnder100cmType`: Unidad de medida utilizada para mostrar el valor de la temperatura bajo 100 cm, expresado en grados Celsius (ºC).
- `temperatureUnder10cmType`: Unidad de medida utilizada para mostrar el valor de la temperatura bajo 10 cm, expresado en grados Celsius (ºC).
- `temperatureUnder20cmType`: Unidad de medida utilizada para mostrar el valor de la temperatura bajo 20 cm, expresado en grados Celsius (ºC).
- `temperatureUnder50cmType`: Unidad de medida utilizada para mostrar el valor de la temperatura bajo 50 cm, expresado en grados Celsius (ºC).
- `temperatureUnder5cmType`: Unidad de medida utilizada para mostrar el valor de la temperatura bajo 5 cm, expresado en grados Celsius (ºC).
- `relativeHumidityType`: Unidad de medida para mostrar el valor de la humedad relativa.
- `windDirectionType`: Unidad de medida para mostrar el valor de la dirección del viento.
- `windDirectionMaxType`: Unidad de medida para mostrar el valor de la dirección máxima del viento.
- `windSpeedType`: Unidad de medida para mostrar el valor de la velocidad del viento.
- `windSpeedUltraType`: Unidad de medida utilizada para mostrar el valor de la velocidad del viento ultra, expresado en metros por segundo (m/s).
- `windSpeedMaxType`: Unidad de medida utilizada para mostrar el valor máximo de la velocidad del viento, expresado en metros por segundo (m/s).
- `windSpeedDesviationType`: Unidad de medida utilizada para mostrar el valor de la desviación de la velocidad del viento, expresado en metros por segundo (m/s).
- `windSpeedMaxUltraType`: Unidad de medida utilizada para mostrar el valor máximo ultra de la velocidad del viento, expresado en metros por segundo (m/s).
- `windSpeedDesviationUltraType`: Unidad de medida utilizada para mostrar el valor de la desviación ultra de la velocidad del viento, expresado en metros por segundo (m/s).
- `pressureTendencyType`: Unidad de medida utilizada para mostrar el valor de la tendencia de presión, expresado como número.
- `windPathType`: Unidad de medida utilizada para mostrar el valor del camino del viento, expresado en hectómetros (Hm).
- `geopotentialHeightType`: Unidad de medida utilizada para mostrar el valor de la altura geopotencial, expresado en metros (m).
- `reductionPressure700Type`: Unidad de medida utilizada para mostrar el valor de la reducción de presión a 700 hPa, expresado en metros (m).
- `reductionPressure850Type`: Unidad de medida utilizada para mostrar el valor de la reducción de presión a 850 hPa, expresado en metros (m).
- `reductionPressure925Type`: Unidad de medida utilizada para mostrar el valor de la reducción de presión a 925 hPa, expresado en metros (m).
- `steamTensionType`: Unidad de medida utilizada para mostrar el valor de la tensión de vapor, expresado en hectopascales (hPa).
- `insolationType`: Unidad de medida utilizada para mostrar el valor de la insolación, expresado como número.
- `cloudCoverType`: Unidad de medida utilizada para mostrar el valor de la cobertura de nubes, expresado en porcentaje (%).
- `visibilityType`: Unidad de medida utilizada para mostrar el valor de la visibilidad, expresado en kilómetros (Km).
- `snowHeightType`: Unidad de medida utilizada para mostrar el valor de la altura de nieve, expresado en centímetros (cm).
- `snowHeightEstimatedValue`: Unidad de medida utilizada para mostrar el valor estimado de la altura de nieve, expresado en centímetros (cm).
- `lightningType`: Unidad de medida utilizada para mostrar el valor de la actividad de relámpagos, expresado como número.

### Atributos para realizar cálculos / lógica de negocio

- `gis_id`: Nombre identificador en el esquema GIS del PostgreSQL municipal.

### Atributos para el GIS / Representar gráficamente la entidad

- Ninguno

### Atributos para la monitorización

- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData.
- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad                 |
| -------------- | -------------------------- |
| /MedioAmbiente | W03_VALENCIAAEROPUERTO_10m |
| /MedioAmbiente | W04_VALENCIADT_10m         |
| /MedioAmbiente | W05_VALENCIA_UPV_10m       |

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "xxxxx",
  "url": "https://xxxxxxxxxxxxxxxxxxxx/",
  "headers": {
    "host": "xxxxxxxxxxxxxxxxxxxx",
    "content-length": "13780",
    "user-agent": "orion/3.10.1 libcurl/7.74.0",
    "fiware-service": "xxxx",
    "fiware-servicepath": "/xxxxx",
    "x-auth-token": "xxxxxxxxxxxxxxxxxxxx",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "xxxxx; cbnotif=1",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"xxxxx\",\"data\":[{\"id\":\"prueba001\",\"type\":\"WeatherObserved\",\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\",\"metadata\":{}},\"altitude\":{\"type\":\"Number\",\"value\":50,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.786Z\"}}},\"atmosphericNmarPressureType\":{\"type\":\"Text\",\"value\":\"hPa\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"atmosphericPressureNMARValue\":{\"type\":\"Number\",\"value\":1014.8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"atmosphericPressureType\":{\"type\":\"Text\",\"value\":\"hPa\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"atmosphericPressureValue\":{\"type\":\"Number\",\"value\":1015.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"cloudCoverType\":{\"type\":\"Text\",\"value\":\"%\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"cloudCoverValue\":{\"type\":\"Number\",\"value\":400,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.786Z\"}}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2023-11-14T12:00:00.000Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"geopotentialHeightType\":{\"type\":\"Text\",\"value\":\"m\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"geopotentialHeightValue\":{\"type\":\"Number\",\"value\":850,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"gis_id\":{\"type\":\"Text\",\"value\":\"WS_001\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"insolationType\":{\"type\":\"Text\",\"value\":\"Numero\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"insolationValue\":{\"type\":\"Number\",\"value\":500,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"lightningType\":{\"type\":\"Text\",\"value\":\"Numero\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"lightningValue\":{\"type\":\"Number\",\"value\":0.2,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-73.987,40.756],\"type\":\"Point\"},\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"xxxxx\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"xxxxx@xxx.es\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"name\":{\"type\":\"Text\",\"value\":\"121\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"Operational\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationAccumulatedLiquidType\":{\"type\":\"Text\",\"value\":\"mm\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationAccumulatedLiquidValue\":{\"type\":\"Number\",\"value\":6.2,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationAccumulatedSolidType\":{\"type\":\"Text\",\"value\":\"%\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationAccumulatedSolidValue\":{\"type\":\"Number\",\"value\":2.3,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationAccumulatedType\":{\"type\":\"Text\",\"value\":\"mm\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationAccumulatedValue\":{\"type\":\"Number\",\"value\":8.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationType\":{\"type\":\"Text\",\"value\":\"mm\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"precipitationValue\":{\"type\":\"Number\",\"value\":2.3,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"pressureTendencyType\":{\"type\":\"Text\",\"value\":\"Numero\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"pressureTendencyValue\":{\"type\":\"Number\",\"value\":0.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"project\":{\"type\":\"Text\",\"value\":\"xxxxx\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"reductionPressure700Type\":{\"type\":\"Text\",\"value\":\"m\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"reductionPressure700Value\":{\"type\":\"Number\",\"value\":200,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"reductionPressure850Type\":{\"type\":\"Text\",\"value\":\"m\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"reductionPressure850Value\":{\"type\":\"Number\",\"value\":150,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"reductionPressure925Type\":{\"type\":\"Text\",\"value\":\"m\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"reductionPressure925Value\":{\"type\":\"Number\",\"value\":100,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"City Center\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"relativeHumidityType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"relativeHumidityValue\":{\"type\":\"Number\",\"value\":70.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"xxxxx\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"xx@xxx.es\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"snowHeightEstimatedValue\":{\"type\":\"Number\",\"value\":3,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"snowHeightType\":{\"type\":\"Text\",\"value\":\"cm\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"snowHeightValue\":{\"type\":\"Number\",\"value\":5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"steamTensionType\":{\"type\":\"Text\",\"value\":\"hPa\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"steamTensionValue\":{\"type\":\"Number\",\"value\":15,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperaturaMax10mType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperaturaSoilValue\":{\"type\":\"Number\",\"value\":18.7,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureDewPointType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureDewPointValue\":{\"type\":\"Number\",\"value\":20.2,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMax10mValue\":{\"type\":\"Number\",\"value\":27.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMax1hType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMax1hValue\":{\"type\":\"Number\",\"value\":26.8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMim10mValue\":{\"type\":\"Number\",\"value\":23.8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMin10mType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMin1hType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureMin1hValue\":{\"type\":\"Number\",\"value\":24.3,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureSoilType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder100cmType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder100cmValue\":{\"type\":\"Number\",\"value\":15.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder10cmType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder10cmValue\":{\"type\":\"Number\",\"value\":20.2,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder20cmType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder20cmValue\":{\"type\":\"Number\",\"value\":18.9,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder50cmType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder50cmValue\":{\"type\":\"Number\",\"value\":17.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder5cmType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureUnder5cmValue\":{\"type\":\"Number\",\"value\":22,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"temperatureValue\":{\"type\":\"Number\",\"value\":25.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"visibilityType\":{\"type\":\"Text\",\"value\":\"km\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"visibilityValue\":{\"type\":\"Number\",\"value\":10,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionDesviationMaxType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionDesviationValue\":{\"type\":\"Number\",\"value\":10,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionMaxType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionMaxUltraType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionMaxUltraValue\":{\"type\":\"Number\",\"value\":125,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionMaxValue\":{\"type\":\"Number\",\"value\":135,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionUltraDesviationType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionUltraDesviationValue\":{\"type\":\"Number\",\"value\":8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionUltraType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionUltraValue\":{\"type\":\"Number\",\"value\":110,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windDirectionValue\":{\"type\":\"Number\",\"value\":120,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windPathType\":{\"type\":\"Text\",\"value\":\"hm\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windPathValue\":{\"type\":\"Number\",\"value\":180,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedDesviationType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedDesviationUltraType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedDesviationUltraValue\":{\"type\":\"Number\",\"value\":1.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedDesviationValue\":{\"type\":\"Number\",\"value\":2,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedMaxType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedMaxUltraType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedMaxValue\":{\"type\":\"Number\",\"value\":12,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedUltraMaxValue\":{\"type\":\"Number\",\"value\":10.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedUltraType\":{\"type\":\"Text\",\"value\":\"m/s\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedUltraValue\":{\"type\":\"Number\",\"value\":7.8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}},\"windSpeedValue\":{\"type\":\"Number\",\"value\":8.5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2023-11-30T11:59:41.787Z\"}}}}]}",
  "body": {
    "subscriptionId": "xxxxx",
    "data": [
      {
        "id": "prueba001",
        "type": "WeatherObserved",
        "TimeInstant": {
          "type": "DateTime",
          "value": "2023-11-30T11:59:41.787Z",
          "metadata": {}
        },
        "altitude": {
          "type": "Number",
          "value": 50,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.786Z"
            }
          }
        },
        "atmosphericNmarPressureType": {
          "type": "Text",
          "value": "hPa",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "atmosphericPressureNMARValue": {
          "type": "Number",
          "value": 1014.8,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "atmosphericPressureType": {
          "type": "Text",
          "value": "hPa",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "atmosphericPressureValue": {
          "type": "Number",
          "value": 1015.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "cloudCoverType": {
          "type": "Text",
          "value": "%",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "cloudCoverValue": {
          "type": "Number",
          "value": 400,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.786Z"
            }
          }
        },
        "dateObserved": {
          "type": "DateTime",
          "value": "2023-11-14T12:00:00.000Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "geopotentialHeightType": {
          "type": "Text",
          "value": "m",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "geopotentialHeightValue": {
          "type": "Number",
          "value": 850,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "gis_id": {
          "type": "Text",
          "value": "WS_001",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "insolationType": {
          "type": "Text",
          "value": "Numero",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "insolationValue": {
          "type": "Number",
          "value": 500,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "lightningType": {
          "type": "Text",
          "value": "Numero",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "lightningValue": {
          "type": "Number",
          "value": 0.2,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "location": {
          "type": "geo:json",
          "value": {
            "coordinates": [
              -73.987,
              40.756
            ],
            "type": "Point"
          },
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "xxxxx",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "xxxxx@xxx.es",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "name": {
          "type": "Text",
          "value": "121",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "operationalStatus": {
          "type": "Text",
          "value": "Operational",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationAccumulatedLiquidType": {
          "type": "Text",
          "value": "mm",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationAccumulatedLiquidValue": {
          "type": "Number",
          "value": 6.2,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationAccumulatedSolidType": {
          "type": "Text",
          "value": "%",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationAccumulatedSolidValue": {
          "type": "Number",
          "value": 2.3,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationAccumulatedType": {
          "type": "Text",
          "value": "mm",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationAccumulatedValue": {
          "type": "Number",
          "value": 8.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationType": {
          "type": "Text",
          "value": "mm",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "precipitationValue": {
          "type": "Number",
          "value": 2.3,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "pressureTendencyType": {
          "type": "Text",
          "value": "Numero",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "pressureTendencyValue": {
          "type": "Number",
          "value": 0.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "project": {
          "type": "Text",
          "value": "xxxxx",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "reductionPressure700Type": {
          "type": "Text",
          "value": "m",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "reductionPressure700Value": {
          "type": "Number",
          "value": 200,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "reductionPressure850Type": {
          "type": "Text",
          "value": "m",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "reductionPressure850Value": {
          "type": "Number",
          "value": 150,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "reductionPressure925Type": {
          "type": "Text",
          "value": "m",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "reductionPressure925Value": {
          "type": "Number",
          "value": 100,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "refPointOfInterest": {
          "type": "Text",
          "value": "City Center",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "relativeHumidityType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "relativeHumidityValue": {
          "type": "Number",
          "value": 70.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "serviceOwner": {
          "type": "Text",
          "value": "xxxxx",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "xx@xxx.es",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "snowHeightEstimatedValue": {
          "type": "Number",
          "value": 3,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "snowHeightType": {
          "type": "Text",
          "value": "cm",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "snowHeightValue": {
          "type": "Number",
          "value": 5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "steamTensionType": {
          "type": "Text",
          "value": "hPa",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "steamTensionValue": {
          "type": "Number",
          "value": 15,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperaturaMax10mType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperaturaSoilValue": {
          "type": "Number",
          "value": 18.7,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureDewPointType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureDewPointValue": {
          "type": "Number",
          "value": 20.2,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMax10mValue": {
          "type": "Number",
          "value": 27.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMax1hType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMax1hValue": {
          "type": "Number",
          "value": 26.8,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMim10mValue": {
          "type": "Number",
          "value": 23.8,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMin10mType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMin1hType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureMin1hValue": {
          "type": "Number",
          "value": 24.3,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureSoilType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder100cmType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder100cmValue": {
          "type": "Number",
          "value": 15.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder10cmType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder10cmValue": {
          "type": "Number",
          "value": 20.2,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder20cmType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder20cmValue": {
          "type": "Number",
          "value": 18.9,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder50cmType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder50cmValue": {
          "type": "Number",
          "value": 17.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder5cmType": {
          "type": "Text",
          "value": "ºC",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureUnder5cmValue": {
          "type": "Number",
          "value": 22,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "temperatureValue": {
          "type": "Number",
          "value": 25.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "visibilityType": {
          "type": "Text",
          "value": "km",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "visibilityValue": {
          "type": "Number",
          "value": 10,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionDesviationMaxType": {
          "type": "Text",
          "value": "grados",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionDesviationValue": {
          "type": "Number",
          "value": 10,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionMaxType": {
          "type": "Text",
          "value": "grados",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionMaxUltraType": {
          "type": "Text",
          "value": "grados",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionMaxUltraValue": {
          "type": "Number",
          "value": 125,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionMaxValue": {
          "type": "Number",
          "value": 135,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionType": {
          "type": "Text",
          "value": "grados",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionUltraDesviationType": {
          "type": "Text",
          "value": "grados",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionUltraDesviationValue": {
          "type": "Number",
          "value": 8,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionUltraType": {
          "type": "Text",
          "value": "grados",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionUltraValue": {
          "type": "Number",
          "value": 110,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windDirectionValue": {
          "type": "Number",
          "value": 120,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windPathType": {
          "type": "Text",
          "value": "hm",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windPathValue": {
          "type": "Number",
          "value": 180,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedDesviationType": {
          "type": "Text",
          "value": "m/s",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedDesviationUltraType": {
          "type": "Text",
          "value": "m/s",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedDesviationUltraValue": {
          "type": "Number",
          "value": 1.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedDesviationValue": {
          "type": "Number",
          "value": 2,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedMaxType": {
          "type": "Text",
          "value": "m/s",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedMaxUltraType": {
          "type": "Text",
          "value": "m/s",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedMaxValue": {
          "type": "Number",
          "value": 12,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedType": {
          "type": "Text",
          "value": "m/s",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedUltraMaxValue": {
          "type": "Number",
          "value": 10.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedUltraType": {
          "type": "Text",
          "value": "m/s",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedUltraValue": {
          "type": "Number",
          "value": 7.8,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        },
        "windSpeedValue": {
          "type": "Number",
          "value": 8.5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2023-11-30T11:59:41.787Z"
            }
          }
        }
      }
    ]
  }
}

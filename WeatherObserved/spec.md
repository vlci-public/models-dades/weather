**Índice**  

 [[_TOC_]]  
  

# Entidad: WeatherObserved

[Basado en la entidad Fiware WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherObserved)

Descripción global: **Una observación de las condiciones del clima en un lugar y momento determinados.**  

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker
- `id[string]`: Identificador único de la entidad.
- `type[string]`: NGSI Tipo de entidad.

##### Atributos descriptivos de la entidad
- `name[string]`: El nombre de este artículo.
- `location[geo:json]`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `address[string]`: La direccion postal.
- `project[string]`: Proyecto al que pertenece esta entidad.
- `description[string]`: Una descripción de este artículo.

##### Atributos descriptivos de la entidad (OPCIONALES)
- `dataProvider[string]`: Una secuencia de caracteres que identifica al proveedor de la entidad de datos armonizada.
- `refPointOfInterest[string]`: Una referencia a un punto de interés (normalmente una estación de calidad del aire) asociada a esta observación.  

##### Atributos de la medición
- `dateObserved[dateTime]`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: Europe/Madrid.
- `dateObservedGMT0[dateTime]`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: UTC o Hora Zulu o GMT+0.
- `atmosphericPressure[number]`: La presión atmosférica observada. Se mide en hPa.
- `precipitation[number]`: Nivel de precipitación observado. Se mide en mm.
- `relativeHumidity[number]`: Humedad relativa del aire observada. Se mide en %.
- `temperature[number]`: Temperatura del aire observada. Se mide en ºC.
- `windDirection[number]`: La dirección del viento observada. Se mide en º.
- `windSpeed[number]`: La velocidad del viento observada. Se mide en m/s.
- `solarRadiation[number]`: La radiación solar observada. Se mide en w/m2.
- `uVIndexMax[number]`: El índice ultravioleta observada. Se mide en uVIndex. Basado en la medida del [índice UV de la Organización Mundial de la Salud](http://www.who.int/uv/intersunprogramme/activities/uv_index/en/). Los valores entre 1 y 11 son el rango válido para el índice. El valor 0 es para describir que no se detecta ninguna señal por lo que no se almacena ningún valor.


##### Atributos para realizar cálculos / lógica de negocio

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus[string]`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData
- `inactive[boolean]`: Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner[string]`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail[string]`: Email del responsable técnico de esta entidad
- `serviceOwner[string]`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail[string]`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos innecesarios en futuras entidades
- Ninguno

*No existe ninguna entidad por el momento. Los siguientes apartado se rellenarán cuando tengamos ejemplos que representar.

## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad        |
|----------------|-------------------|
| /              |                   |
| /              |                   |



##### Ejemplo de entidad

```

```
##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

Atributos que van al GIS para su representación en una capa
===========================
- Ninguno. No existe suscripción al GIS para estas entidades

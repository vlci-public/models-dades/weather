**Índice**  

 [[_TOC_]]  
  

# Entidad: WeatherObserved

[Basado en la entidad Fiware WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherObserved)

Descripción global: **Una observación de las condiciones del clima en un lugar y momento determinados.**  

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad.
- `type`: NGSI Tipo de entidad.

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `description`: Una descripción de este artículo.
- `refPointOfInterest`: Una referencia a un punto de interés (normalmente una estación de clima) asociada a esta observación.

##### Atributos descriptivos de la entidad (OPCIONALES)
- Ninguno

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: Europe/Madrid.
- `dateObservedGMT0`: La fecha y la hora de esta observación en formato ISO8601, TimeZone: UTC o Hora Zulu o GMT+0.
- `atmosphericPressureValue`: La presión atmosférica observada.
- `atmosphericPressureValueFlag`: Indica si el valor de presión atmosférica es válido o no. El valor esperado es 'V' para darlo por correcto.
- `precipitationValue`: Nivel de precipitación observado.
- `precipitationValueFlag`: Indica si el valor de precipitación es válido o no. El valor esperado es 'V' para darlo por correcto.
- `relativeHumidityValue`: Humedad relativa del aire observada.
- `relativeHumidityValueFlag`: Indica si el valor de humedad relativa es válido o no. El valor esperado es 'V' para darlo por correcto.
- `temperatureValue`: Temperatura del aire observada.
- `temperatureValueFlag`: Indica si el valor de temperatura es válido o no. El valor esperado es 'V' para darlo por correcto.
- `windDirectionValue`: La dirección del viento observada.
- `windDirectionValueFlag`: Indica si el valor de dirección del viento es válido o no. El valor esperado es 'V' para darlo por correcto.
- `windSpeedValue`: La velocidad del viento observada.
- `windSpeedValueFlag`: Indica si el valor de velocidad del viento es válido o no. El valor esperado es 'V' para darlo por correcto.

##### Atributos descriptivos de la medición
- `atmosphericPressureDescription`: Una descripción para la presión atmosférica.
- `atmosphericPressureName`: Un nombre para la presión atmosférica.
- `atmosphericPressureType`: Unidad de medida utilizada para mostrar el valor de la presión atmoférica.
- `precipitationDescription`: Una descripción para la precipitación.
- `precipitationName`: Un nombre para la precipitación.
- `precipitationType`: Unidad de medida utilizada para mostrar el valor de la precipitación.
- `relativeHumidityDescription`: Una descripción para la humedad relativa.
- `relativeHumidityName`: Un nombre para la humedad relativa.
- `relativeHumidityType`: Unidad de medida utilizada para mostrar el valor de la humedad relativa.
- `temperatureDescription`: Una descripción para la temperatura.
- `temperatureName`: Un nombre para la temperatura.
- `temperatureType`: Unidad de medida utilizada para mostrar el valor de temperatura.
- `windDirectionDescription`: Una descripción para la dirección del viento.
- `windDirectionName`: Un nombre para la dirección del viento.
- `windDirectionType`: Unidad de medida utilizada para mostrar el valor de la dirección del viento.
- `windSpeedDescription`: Una descripción para la dirección del viento.
- `windSpeedName`: Un nombre para la dirección del viento.
- `windSpeedType`: Unidad de medida utilizada para mostrar el valor de la velocidad del viento.

##### Atributos para realizar cálculos / lógica de negocio
- `gis_id`: Nombre identificador en el esquema gis del postgres municipal.

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData.
- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad        |
|----------------|-------------------|
| /MedioAmbiente | W01_AVFRANCIA_10m |
| /MedioAmbiente | W02_NAZARET_10m   |


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/W01_AVFRANCIA_10m' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```
##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
    "method": "POST",
    "path": "/",
    "query": {},
    "client_ip": "XX.XX.XX.XX",
    "url": "XXXXX",
    "headers": {
        "host": "XXXXX",
        "content-length": "3080",
        "user-agent": "orion/3.7.0 libcurl/7.74.0",
        "fiware-service": "XXXXX",
        "fiware-servicepath": "/MedioAmbiente",
        "x-auth-token": "XXXXX",
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "fiware-correlator": "ca384778-fe21-402d-b4b3-8f3dfeef935d; cbnotif=6",
        "ngsiv2-attrsformat": "normalized"
    },
    "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"W01_AVFRANCIA_10m\",\"type\":\"WeatherObserved\",\"atmosphericPressureDescription\":{\"type\":\"Text\",\"value\":\"Presión atmosférica\",\"metadata\":{}},\"atmosphericPressureName\":{\"type\":\"Text\",\"value\":\"atmosphericPressure\",\"metadata\":{}},\"atmosphericPressureType\":{\"type\":\"Text\",\"value\":\"bares\",\"metadata\":{}},\"atmosphericPressureValue\":{\"type\":\"Number\",\"value\":\"1013.0\",\"metadata\":{}},\"atmosphericPressureValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-07T08:50:00.000Z\",\"metadata\":{}},\"dateObservedGMT0\":{\"type\":\"DateTime\",\"value\":\"2022-09-07T06:50:00.000Z\",\"metadata\":{}},\"gis_id\":{\"type\":\"Text\",\"value\":\"ESTACIÓN AVD. FRANCIA\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.342666,39.457526],\"type\":\"Point\"},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"precipitationDescription\":{\"type\":\"Text\",\"value\":\"Precipitaciones\",\"metadata\":{}},\"precipitationName\":{\"type\":\"Text\",\"value\":\"precipitation\",\"metadata\":{}},\"precipitationType\":{\"type\":\"Text\",\"value\":\"l/m2\",\"metadata\":{}},\"precipitationValue\":{\"type\":\"Number\",\"value\":\"0.0\",\"metadata\":{}},\"precipitationValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"W01_AVFRANCIA\",\"metadata\":{}},\"relativeHumidityDescription\":{\"type\":\"Text\",\"value\":\"Humedad relativa\",\"metadata\":{}},\"relativeHumidityName\":{\"type\":\"Text\",\"value\":\"relativeHumidity\",\"metadata\":{}},\"relativeHumidityType\":{\"type\":\"Text\",\"value\":\"%\",\"metadata\":{}},\"relativeHumidityValue\":{\"type\":\"Number\",\"value\":\"42.0\",\"metadata\":{}},\"relativeHumidityValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"temperatureDescription\":{\"type\":\"Text\",\"value\":\"Temperatura ambiental\",\"metadata\":{}},\"temperatureName\":{\"type\":\"Text\",\"value\":\"temperature\",\"metadata\":{}},\"temperatureType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{}},\"temperatureValue\":{\"type\":\"Number\",\"value\":\"27.9\",\"metadata\":{}},\"temperatureValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"windDirectionDescription\":{\"type\":\"Text\",\"value\":\"Dirección del viento\",\"metadata\":{}},\"windDirectionName\":{\"type\":\"Text\",\"value\":\"windDirection\",\"metadata\":{}},\"windDirectionType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{}},\"windDirectionValue\":{\"type\":\"Number\",\"value\":\"269.0\",\"metadata\":{}},\"windDirectionValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"windSpeedDescription\":{\"type\":\"Text\",\"value\":\"Velocidad del viento\",\"metadata\":{}},\"windSpeedName\":{\"type\":\"Text\",\"value\":\"windSpeed\",\"metadata\":{}},\"windSpeedType\":{\"type\":\"Text\",\"value\":\"km/h\",\"metadata\":{}},\"windSpeedValue\":{\"type\":\"Number\",\"value\":\"0.2\",\"metadata\":{}},\"windSpeedValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}}}]}",
    "body": {
        "subscriptionId": "XXXXX",
        "data": [
            {
                "id": "W01_AVFRANCIA_10m",
                "type": "WeatherObserved",
                "atmosphericPressureDescription": {
                    "type": "Text",
                    "value": "Presión atmosférica",
                    "metadata": {}
                },
                "atmosphericPressureName": {
                    "type": "Text",
                    "value": "atmosphericPressure",
                    "metadata": {}
                },
                "atmosphericPressureType": {
                    "type": "Text",
                    "value": "bares",
                    "metadata": {}
                },
                "atmosphericPressureValue": {
                    "type": "Number",
                    "value": "1013.0",
                    "metadata": {}
                },
                "atmosphericPressureValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "dateObserved": {
                    "type": "DateTime",
                    "value": "2022-09-07T08:50:00.000Z",
                    "metadata": {}
                },
                "dateObservedGMT0": {
                    "type": "DateTime",
                    "value": "2022-09-07T06:50:00.000Z",
                    "metadata": {}
                },
                "gis_id": {
                    "type": "Text",
                    "value": "ESTACIÓN AVD. FRANCIA",
                    "metadata": {}
                },
                "location": {
                    "type": "geo:json",
                    "value": {
                        "coordinates": [
                            -0.342666,
                            39.457526
                        ],
                        "type": "Point"
                    },
                    "metadata": {}
                },
                "maintenanceOwner": {
                    "type": "Text",
                    "value": "OCI",
                    "metadata": {}
                },
                "maintenanceOwnerEmail": {
                    "type": "Text",
                    "value": "XXXXX",
                    "metadata": {}
                },
                "operationalStatus": {
                    "type": "Text",
                    "value": "ok",
                    "metadata": {}
                },
                "precipitationDescription": {
                    "type": "Text",
                    "value": "Precipitaciones",
                    "metadata": {}
                },
                "precipitationName": {
                    "type": "Text",
                    "value": "precipitation",
                    "metadata": {}
                },
                "precipitationType": {
                    "type": "Text",
                    "value": "l/m2",
                    "metadata": {}
                },
                "precipitationValue": {
                    "type": "Number",
                    "value": "0.0",
                    "metadata": {}
                },
                "precipitationValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "refPointOfInterest": {
                    "type": "Text",
                    "value": "W01_AVFRANCIA",
                    "metadata": {}
                },
                "relativeHumidityDescription": {
                    "type": "Text",
                    "value": "Humedad relativa",
                    "metadata": {}
                },
                "relativeHumidityName": {
                    "type": "Text",
                    "value": "relativeHumidity",
                    "metadata": {}
                },
                "relativeHumidityType": {
                    "type": "Text",
                    "value": "%",
                    "metadata": {}
                },
                "relativeHumidityValue": {
                    "type": "Number",
                    "value": "42.0",
                    "metadata": {}
                },
                "relativeHumidityValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "serviceOwner": {
                    "type": "Text",
                    "value": "OCI",
                    "metadata": {}
                },
                "serviceOwnerEmail": {
                    "type": "Text",
                    "value": "XXXXX",
                    "metadata": {}
                },
                "temperatureDescription": {
                    "type": "Text",
                    "value": "Temperatura ambiental",
                    "metadata": {}
                },
                "temperatureName": {
                    "type": "Text",
                    "value": "temperature",
                    "metadata": {}
                },
                "temperatureType": {
                    "type": "Text",
                    "value": "ºC",
                    "metadata": {}
                },
                "temperatureValue": {
                    "type": "Number",
                    "value": "27.9",
                    "metadata": {}
                },
                "temperatureValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "windDirectionDescription": {
                    "type": "Text",
                    "value": "Dirección del viento",
                    "metadata": {}
                },
                "windDirectionName": {
                    "type": "Text",
                    "value": "windDirection",
                    "metadata": {}
                },
                "windDirectionType": {
                    "type": "Text",
                    "value": "grados",
                    "metadata": {}
                },
                "windDirectionValue": {
                    "type": "Number",
                    "value": "269.0",
                    "metadata": {}
                },
                "windDirectionValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "windSpeedDescription": {
                    "type": "Text",
                    "value": "Velocidad del viento",
                    "metadata": {}
                },
                "windSpeedName": {
                    "type": "Text",
                    "value": "windSpeed",
                    "metadata": {}
                },
                "windSpeedType": {
                    "type": "Text",
                    "value": "km/h",
                    "metadata": {}
                },
                "windSpeedValue": {
                    "type": "Number",
                    "value": "0.2",
                    "metadata": {}
                },
                "windSpeedValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                }
            }
        ]
    }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- El GIS se actualiza mediante una ETL que accede al Postgres.

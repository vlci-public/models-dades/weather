**Table of contents**  

 [[_TOC_]]  
  

# Entidad: WeatherObserved

[Based upon the Fiware Entity WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/tree/master/WeatherObserved)

General descrption: **An observation of weather conditions at a certain place and time.**

Uses best practices and standards defined [here](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/blob/main/contextbroker/README.md).

## Properties List

##### Context broker entity Attributes

- `id`: Unique identifier of the entity
- `type`: NGSI Entity Type

##### Entity description attributes

- `name`: The name of this item
- `location`: Geojson reference to the item. It can be Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
- `description`: A description of this item
- `refPointOfInterest`: Entity ID, refers to a PointOfInterest entity type, wich describes the station

##### (OPTIONAL) Entity description attributes

- None

##### Measurement attributes

- `dateObserved`: The date and time of the measurement in ISO8601 format, converted to the Europe/Madrid timezone
- `dateObservedGMT0`: The date and time of this observation in ISO8601 format, in the UTC/GMT+0 Timezone.
- `atmosphericPressureValue`: The atmospheric pressure observed
- `atmosphericPressureValueFlag`: Indicates wether the atmospheric pressure value is valid or not, only a 'V' value indicates a valid measurement
- `precipitationValue`: Amount of water rain registered
- `precipitationValueFlag`: Indicates wether the water rain value is valid or not, only a 'V' value indicates a valid measurement
- `relativeHumidityValue`: Humidity in the Air. Observed instantaneous relative humidity (water vapour in air)
- `relativeHumidityValueFlag`: Indicates wether the humidity value is valid or not, only a 'V' value indicates a valid measurement
- `temperatureValue`: Temperature measured.
- `temperatureValueFlag`: Indicates wether the temperature value is valid or not, only a 'V' value indicates a valid measurement
- `windDirectionValue`: Direction of the wind bet
- `windDirectionValueFlag`: Indicates wether the wind direction value is valid or not, only a 'V' value indicates a valid meassurement
- `windSpeedValue`: Intensity of the wind
- `windSpeedValueFlag`: Indicates wether the intensity of the wind value is valid or not, only a 'V' value indicates a valid meassurement

##### Measurement Description attributes

- `atmosphericPressureDescription`: Atmospheric pressure description
- `atmosphericPressureName`: Atmospheric pressure name
- `atmosphericPressureType`: Atmospheric pressure measurement unit
- `precipitationDescription`: Precipitation description
- `precipitationName`: Precipitation name
- `precipitationType`: Precipitation peasurement unit
- `relativeHumidityDescription`: Humidity in the air description
- `relativeHumidityName`: Humidity name
- `relativeHumidityType`: Humidity measurement unit
- `temperatureDescription`: Temperature description
- `temperatureName`: Temperature name
- `temperatureType`: Temperature measurement unit
- `windDirectionDescription`: Wind direction description
- `windDirectionName`: Wind direction name
- `windDirectionType`: Wind direction measurement unit
- `windSpeedDescription`: Wind speed description
- `windSpeedName`: Wind speed name
- `windSpeedType`: Wind speed measurement unit

##### Attributes to be used in calculations / Business logic

- `gis_id`: Identifying name in the municipal postgres schema

##### GIS attributes / Entity graphical representation

- None

##### Monitoring Attributes

- `operationalStatus`: Possible values: ok, noData
- `maintenanceOwner`: Technical manager for this entity
- `maintenanceOwnerEmail`: Mailing address from the technical manager
- `serviceOwner`: Municipal worker in charge of the entity
- `serviceOwnerEmail`: Mailing address from the municipal worker

##### Unnecessary attributes in future implementations

- None

## Entity list

| Sub service    | Entity Id         |
| -------------- | ----------------- |
| /MedioAmbiente | W01_AVFRANCIA_10m |
| /MedioAmbiente | W02_NAZARET_10m   |

##### Entity example

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/W01_AVFRANCIA_10m' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>
{
    "method": "POST",
    "path": "/",
    "query": {},
    "client_ip": "XX.XX.XX.XX",
    "url": "XXXXX",
    "headers": {
        "host": "XXXXX",
        "content-length": "3080",
        "user-agent": "orion/3.7.0 libcurl/7.74.0",
        "fiware-service": "XXXXX",
        "fiware-servicepath": "/MedioAmbiente",
        "x-auth-token": "XXXXX",
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "fiware-correlator": "ca384778-fe21-402d-b4b3-8f3dfeef935d; cbnotif=6",
        "ngsiv2-attrsformat": "normalized"
    },
    "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"W01_AVFRANCIA_10m\",\"type\":\"WeatherObserved\",\"atmosphericPressureDescription\":{\"type\":\"Text\",\"value\":\"Presión atmosférica\",\"metadata\":{}},\"atmosphericPressureName\":{\"type\":\"Text\",\"value\":\"atmosphericPressure\",\"metadata\":{}},\"atmosphericPressureType\":{\"type\":\"Text\",\"value\":\"bares\",\"metadata\":{}},\"atmosphericPressureValue\":{\"type\":\"Number\",\"value\":\"1013.0\",\"metadata\":{}},\"atmosphericPressureValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-07T08:50:00.000Z\",\"metadata\":{}},\"dateObservedGMT0\":{\"type\":\"DateTime\",\"value\":\"2022-09-07T06:50:00.000Z\",\"metadata\":{}},\"gis_id\":{\"type\":\"Text\",\"value\":\"ESTACIÓN AVD. FRANCIA\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.342666,39.457526],\"type\":\"Point\"},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"precipitationDescription\":{\"type\":\"Text\",\"value\":\"Precipitaciones\",\"metadata\":{}},\"precipitationName\":{\"type\":\"Text\",\"value\":\"precipitation\",\"metadata\":{}},\"precipitationType\":{\"type\":\"Text\",\"value\":\"l/m2\",\"metadata\":{}},\"precipitationValue\":{\"type\":\"Number\",\"value\":\"0.0\",\"metadata\":{}},\"precipitationValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"W01_AVFRANCIA\",\"metadata\":{}},\"relativeHumidityDescription\":{\"type\":\"Text\",\"value\":\"Humedad relativa\",\"metadata\":{}},\"relativeHumidityName\":{\"type\":\"Text\",\"value\":\"relativeHumidity\",\"metadata\":{}},\"relativeHumidityType\":{\"type\":\"Text\",\"value\":\"%\",\"metadata\":{}},\"relativeHumidityValue\":{\"type\":\"Number\",\"value\":\"42.0\",\"metadata\":{}},\"relativeHumidityValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"temperatureDescription\":{\"type\":\"Text\",\"value\":\"Temperatura ambiental\",\"metadata\":{}},\"temperatureName\":{\"type\":\"Text\",\"value\":\"temperature\",\"metadata\":{}},\"temperatureType\":{\"type\":\"Text\",\"value\":\"ºC\",\"metadata\":{}},\"temperatureValue\":{\"type\":\"Number\",\"value\":\"27.9\",\"metadata\":{}},\"temperatureValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"windDirectionDescription\":{\"type\":\"Text\",\"value\":\"Dirección del viento\",\"metadata\":{}},\"windDirectionName\":{\"type\":\"Text\",\"value\":\"windDirection\",\"metadata\":{}},\"windDirectionType\":{\"type\":\"Text\",\"value\":\"grados\",\"metadata\":{}},\"windDirectionValue\":{\"type\":\"Number\",\"value\":\"269.0\",\"metadata\":{}},\"windDirectionValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"windSpeedDescription\":{\"type\":\"Text\",\"value\":\"Velocidad del viento\",\"metadata\":{}},\"windSpeedName\":{\"type\":\"Text\",\"value\":\"windSpeed\",\"metadata\":{}},\"windSpeedType\":{\"type\":\"Text\",\"value\":\"km/h\",\"metadata\":{}},\"windSpeedValue\":{\"type\":\"Number\",\"value\":\"0.2\",\"metadata\":{}},\"windSpeedValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}}}]}",
    "body": {
        "subscriptionId": "XXXXX",
        "data": [
            {
                "id": "W01_AVFRANCIA_10m",
                "type": "WeatherObserved",
                "atmosphericPressureDescription": {
                    "type": "Text",
                    "value": "Presión atmosférica",
                    "metadata": {}
                },
                "atmosphericPressureName": {
                    "type": "Text",
                    "value": "atmosphericPressure",
                    "metadata": {}
                },
                "atmosphericPressureType": {
                    "type": "Text",
                    "value": "bares",
                    "metadata": {}
                },
                "atmosphericPressureValue": {
                    "type": "Number",
                    "value": "1013.0",
                    "metadata": {}
                },
                "atmosphericPressureValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "dateObserved": {
                    "type": "DateTime",
                    "value": "2022-09-07T08:50:00.000Z",
                    "metadata": {}
                },
                "dateObservedGMT0": {
                    "type": "DateTime",
                    "value": "2022-09-07T06:50:00.000Z",
                    "metadata": {}
                },
                "gis_id": {
                    "type": "Text",
                    "value": "ESTACIÓN AVD. FRANCIA",
                    "metadata": {}
                },
                "location": {
                    "type": "geo:json",
                    "value": {
                        "coordinates": [
                            -0.342666,
                            39.457526
                        ],
                        "type": "Point"
                    },
                    "metadata": {}
                },
                "maintenanceOwner": {
                    "type": "Text",
                    "value": "OCI",
                    "metadata": {}
                },
                "maintenanceOwnerEmail": {
                    "type": "Text",
                    "value": "XXXXX",
                    "metadata": {}
                },
                "operationalStatus": {
                    "type": "Text",
                    "value": "ok",
                    "metadata": {}
                },
                "precipitationDescription": {
                    "type": "Text",
                    "value": "Precipitaciones",
                    "metadata": {}
                },
                "precipitationName": {
                    "type": "Text",
                    "value": "precipitation",
                    "metadata": {}
                },
                "precipitationType": {
                    "type": "Text",
                    "value": "l/m2",
                    "metadata": {}
                },
                "precipitationValue": {
                    "type": "Number",
                    "value": "0.0",
                    "metadata": {}
                },
                "precipitationValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "refPointOfInterest": {
                    "type": "Text",
                    "value": "W01_AVFRANCIA",
                    "metadata": {}
                },
                "relativeHumidityDescription": {
                    "type": "Text",
                    "value": "Humedad relativa",
                    "metadata": {}
                },
                "relativeHumidityName": {
                    "type": "Text",
                    "value": "relativeHumidity",
                    "metadata": {}
                },
                "relativeHumidityType": {
                    "type": "Text",
                    "value": "%",
                    "metadata": {}
                },
                "relativeHumidityValue": {
                    "type": "Number",
                    "value": "42.0",
                    "metadata": {}
                },
                "relativeHumidityValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "serviceOwner": {
                    "type": "Text",
                    "value": "OCI",
                    "metadata": {}
                },
                "serviceOwnerEmail": {
                    "type": "Text",
                    "value": "XXXXX",
                    "metadata": {}
                },
                "temperatureDescription": {
                    "type": "Text",
                    "value": "Temperatura ambiental",
                    "metadata": {}
                },
                "temperatureName": {
                    "type": "Text",
                    "value": "temperature",
                    "metadata": {}
                },
                "temperatureType": {
                    "type": "Text",
                    "value": "ºC",
                    "metadata": {}
                },
                "temperatureValue": {
                    "type": "Number",
                    "value": "27.9",
                    "metadata": {}
                },
                "temperatureValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "windDirectionDescription": {
                    "type": "Text",
                    "value": "Dirección del viento",
                    "metadata": {}
                },
                "windDirectionName": {
                    "type": "Text",
                    "value": "windDirection",
                    "metadata": {}
                },
                "windDirectionType": {
                    "type": "Text",
                    "value": "grados",
                    "metadata": {}
                },
                "windDirectionValue": {
                    "type": "Number",
                    "value": "269.0",
                    "metadata": {}
                },
                "windDirectionValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                },
                "windSpeedDescription": {
                    "type": "Text",
                    "value": "Velocidad del viento",
                    "metadata": {}
                },
                "windSpeedName": {
                    "type": "Text",
                    "value": "windSpeed",
                    "metadata": {}
                },
                "windSpeedType": {
                    "type": "Text",
                    "value": "km/h",
                    "metadata": {}
                },
                "windSpeedValue": {
                    "type": "Number",
                    "value": "0.2",
                    "metadata": {}
                },
                "windSpeedValueFlag": {
                    "type": "Text",
                    "value": "V",
                    "metadata": {}
                }
            }
        ]
    }
}
</details>

# Attributes sent to GIS to be used in graphic representation

- GIS is updated by an ETL that queries a Postgres schema
